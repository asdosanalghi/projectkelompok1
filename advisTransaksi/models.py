from django.db import models
from wening_barang.models import Barang
from nofaldi_kupon.models import Kupon

class Transaksi(models.Model):
    namaPembeli = models.CharField(max_length=100)
    barangDibeli = models.ForeignKey(Barang, on_delete=models.CASCADE,  null=True, blank=True)
    kuponDipakai = models.ForeignKey(Kupon, on_delete=models.CASCADE,  null=True, blank=True)
    totalTransaksi = models.IntegerField(default=0,blank=True,null=True)
    tanggalBeli = models.DateTimeField()
    kuantitasDibeli = models.IntegerField(default=1)

    def __str__ (self):
        return self.namaPembeli

# Create your models here.
