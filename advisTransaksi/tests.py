from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import hasil
from django.urls import reverse
from .models import Transaksi
from wening_barang.models import Barang
from nofaldi_kupon.models import Kupon
from datetime import date

# Create your tests here.
class TestInitial(TestCase):
	def test_event_index_func(self):
		found = resolve('/transaksi/1/')
		self.assertEqual(found.func, hasil)
	
	def test_name(self):
		baruan = Transaksi(namaPembeli="radhi",tanggalBeli=date.today())
		self.assertEqual(str(baruan),baruan.namaPembeli)


class TestTambahTransaksi(TestCase):
	def test_event_model_create_new_object(self):
		acara = Transaksi(namaPembeli="abc", tanggalBeli=date.today())
		acara.save()
		self.assertEqual(Transaksi.objects.all().count(), 1)

class TestBarangHabis(TestCase):
	def setUp(self):
		acara = Barang(stok=0)
		acara.save()

	def test_regist_baranghabis_post_is_exist(self):
		response = Client().post('/transaksi/1/', data={'Nama-Pembeli':'advis', 'Kupon':'123'})
		self.assertEqual(response.status_code, 200)
		response1 = Client().post('/transaksi/1/', data={'Nama-Pembeli':'', 'Kupon':'123'})
		self.assertTemplateUsed(response1,'transaksi.html')
	
	def test_page_delete(self):
		acara = Transaksi.objects.create(id=1,namaPembeli="radhi",tanggalBeli=date.today())
		response = self.client.get(reverse('advisTransaksi:hasil', kwargs={'id': acara.id}))
		self.assertEqual(response.status_code, 200)