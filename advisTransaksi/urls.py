from django.urls import path
from . import views

app_name = 'advisTransaksi'

urlpatterns = [
    path('<id>/', views.hasil, name="hasil"),
]
