from django.shortcuts import render, redirect
from wening_barang.models import Barang
from .models import Transaksi
from nofaldi_kupon.models import Kupon
from datetime import date

def hasil(request,id):
    if request.method == "POST":
        namapembeli = request.POST['Nama-Pembeli']
        if namapembeli == "":
            isi = Transaksi.objects.all()
            stringKosong = "Silahkan isi nama terlebih dahulu"
            beliBarang = Barang.objects.get(id=id)
            content = {
                'isinya' : isi,
                'stringKosong' : stringKosong,
                'namaBarang' : beliBarang.nama,
            }
            return render(request,'transaksi.html',content)
        
        # if request.user.is_anonymous:
        #     return redirect('/login/')

        kuponPembeli = request.POST['Kupon']
        namaBarang = Barang.objects.get(id=id)
        tanggalPembelian = date.today()
        nomorKupon = "No coupon"
        for instance in Kupon.objects.all() :
            if instance.kode == kuponPembeli :
                nomorKupon = instance

        if namaBarang.stok == 0 :
            isi = Transaksi.objects.all()
            beliBarang = Barang.objects.get(id=id)
            stringKosong = "Maaf barang ini sudah habis, silahkan berbelanja item lain"
            content = {
                'isinya' : isi,
                'stringKosong' : stringKosong,
                'namaBarang' : beliBarang.nama,
            }
            return render(request,'transaksi.html',content)

        elif (namaBarang.stok != 0 and nomorKupon == "No coupon") :
            new_entry = Transaksi(namaPembeli=namapembeli,barangDibeli=namaBarang,totalTransaksi=1,tanggalBeli=tanggalPembelian)
            new_entry.save()
            namaBarang.stok -= 1
            namaBarang.save()

            isi = Transaksi.objects.all()
            beliBarang = Barang.objects.get(id=id)
            content = {
                'isinya' : isi,
                'namaBarang' : beliBarang.nama,
            }
            return render(request,'transaksi.html',content)
            
        else:
            new_entry = Transaksi(namaPembeli=namapembeli,barangDibeli=namaBarang,totalTransaksi=1,tanggalBeli=tanggalPembelian,kuponDipakai=nomorKupon)
            new_entry.save()
            namaBarang.stok -= 1
            namaBarang.save()

            isi = Transaksi.objects.all()
            beliBarang = Barang.objects.get(id=id)
            content = {
                'isinya' : isi,
                'namaBarang' : beliBarang.nama,
            }
            return render(request,'transaksi.html',content)

    else:
        isi = Transaksi.objects.all()
        beliBarang = Barang.objects.get(id=id)
        content = {
            'isinya' : isi,
            'namaBarang' : beliBarang.nama,
        }
        return render(request,'transaksi.html',content)