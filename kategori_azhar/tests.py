from django.test import TestCase
from .models import Kategori
from django.urls import reverse

# Create your tests here.
class EntryModelTest(TestCase):
    def test_string_representation(self):
        aktiv = Kategori(nama="My entry title")
        self.assertEqual(str(aktiv), aktiv.nama)
    
    def test_url_exists(self):
        response = self.client.get('/kategori/')
        self.assertEqual(response.status_code,200)

    def test_page_delete(self):
        new_act = Kategori(nama="jalan-jalan")
        response = self.client.post(reverse('kategori-cont', kwargs={'nama': new_act.nama}))
        self.assertEqual(response.status_code, 200)
