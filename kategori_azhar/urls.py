from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.kategori, name = "kategori"),
    path('<str:nama>/', views.kategori_filter, name="kategori-cont"),
]