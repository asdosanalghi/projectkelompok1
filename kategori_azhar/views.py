from django.shortcuts import render
from .models import Kategori
from wening_barang.models import Barang

# Create your views here.

def kategori(request):
    isi = Kategori.objects.all().order_by('nama')
    amount = isi.count()

    return render(request,'kategori.html',{'kategoris':isi, 'jumlah':amount})

def kategori_filter(request, nama):
    filtered = Barang.objects.filter(kategori = nama)
    amount = filtered.count()

    return render(request,'kategori_filter.html',{'isi':filtered, 'kategori': nama, 'jumlah':amount})