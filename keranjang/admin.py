from django.contrib import admin
from .models import Keranjang, BarangOrder

# Register your models here.
admin.site.register(BarangOrder)
admin.site.register(Keranjang)
