# Generated by Django 2.2.12 on 2020-05-15 06:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('nofaldi_kupon', '0001_initial'),
        ('keranjang', '0004_auto_20200515_0203'),
    ]

    operations = [
        migrations.AddField(
            model_name='keranjang',
            name='kupon_used',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='nofaldi_kupon.Kupon'),
        ),
    ]
