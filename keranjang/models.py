from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from wening_barang.models import Barang
from nofaldi_kupon.models import Kupon

# Create your models here.


class BarangOrder(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    kuantitas = models.PositiveIntegerField(
        validators=[MinValueValidator(1)], default=1)
    ordered = models.BooleanField()

    def __str__(self):
        return self.barang.nama

    def harga(self):
        return self.barang.harga


class Keranjang(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    isi_keranjang = models.ManyToManyField(BarangOrder)
    total_harga = models.IntegerField(default=0)
    kupon_used = models.ForeignKey(Kupon, on_delete=models.CASCADE,  null=True, blank=True)

    def __str__(self):
        return "{}'s Shopping Cart".format(self.user.username)
