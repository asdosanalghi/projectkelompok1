$(document).ready(function () {
    $(".tambah").click(function () {
        console.log("Bismillah")
        var namabarang = this.parentNode.parentNode.parentNode.id
        var select = $(this);
        var kuantitas = select.parent().parent().find(".kuantitas");
        var harga = $('body').find("#iniharga");
        var hargabarang = select.parent().parent().parent().find(".harga-barang");
        var persen = $(".jumlahpersen");
        $.ajax({
            url: "/add/",
            data: {
                namabarang: namabarang,
                kuantitas: kuantitas.text()
            },
            type: 'POST',
            success: function (data) {
                var text = kuantitas.text();
                var count = parseInt(text) + 1
                var text_harga_barang = hargabarang.text();
                var text_total_harga = harga.text();
                var text_persen = persen.text();
                var count1 = ((parseInt(text_total_harga)/(1 - (parseInt(text_persen)/100))) + parseInt(text_harga_barang)); //balik ke normal
                var count2 = (parseInt(count1) - (parseInt(count1)*(parseInt(text_persen)/100))); //apply diskon
                kuantitas.text(text.replace(text, count));
                harga.text(text_total_harga.replace(text_total_harga, count2))
                console.log(namabarang)
                console.log(text)
            },
            error: function (data) {
                alert(data.responseJSON.error); // the message
            }
        })
    });

    $(".kurang").click(function () {
        console.log("Bismillah")
        var namabarang = this.parentNode.parentNode.parentNode.id
        var select = $(this);
        var kuantitas = select.parent().parent().find(".kuantitas");
        var harga = $('body').find("#iniharga");
        var hargabarang = select.parent().parent().parent().find(".harga-barang");
        var persen = $(".jumlahpersen");
        $.ajax({
            url: "/reduce/",
            data: {
                namabarang: namabarang,
                kuantitas: kuantitas.text()
            },
            type: 'POST',
            success: function (data) {
                var text = kuantitas.text();
                var count = parseInt(text) - 1
                var text_harga_barang = hargabarang.text();
                var text_total_harga = harga.text();
                var text_persen = persen.text();
                var count1 = ((parseInt(text_total_harga)/(1 - (parseInt(text_persen)/100))) - parseInt(text_harga_barang)); //balik ke normal
                var count2 = (parseInt(count1) - (parseInt(count1)*parseInt(text_persen)/100)); //apply diskon
                kuantitas.text(text.replace(text, count));
                harga.text(text_total_harga.replace(text_total_harga, count2))
                console.log(namabarang)
                console.log(text)
            },
            error: function (data) {
                alert(data.responseJSON.error); // the message
            }
        })
    });

    $(".buang").click(function () {
        console.log("Bismillah")
        var namabarang = this.parentNode.parentNode.id
        var select = $(this);
        var harga = $('body').find("#iniharga");
        var hargabarang = select.parent().parent().find(".harga-barang");
        var kuantitas = select.parent().siblings(".row").find(".kuantitas");
        var persen = $(".jumlahpersen");
        $.ajax({
            url: "/remove/",
            data: {
                namabarang: namabarang,
            },
            type: 'POST',
            success: function (data) {
                console.log(namabarang)
                select.parent().parent("tr").hide();
                var text_harga_barang = hargabarang.text();
                var text_total_harga = harga.text();
                var text_kuantitas = kuantitas.text();
                var text_persen = persen.text();
                var count1 = ((parseInt(text_total_harga)/(1 - (parseInt(text_persen)/100))) - (parseInt(text_harga_barang)*parseInt(text_kuantitas))); //balik ke normal
                var count2 = (parseInt(count1) - (parseInt(count1)*parseInt(text_persen)/100)); //apply diskon
                
                // var count1 = parseInt(text_total_harga) - (parseInt(text_harga_barang)*parseInt(text_kuantitas))
                harga.text(text_total_harga.replace(text_total_harga, count2))
                // window.location.href = data.redirecturl;
            },
            error: function (data) {
                alert(data.responseJSON.error); // the message
            }
        })
    });

    $(".beli").click(function() {
        $("tbody").children().each(function() { // each tr
            var idk = $(this).children();
            var namabarang = idk[1].innerHTML;
            var hargasatuan = parseInt(idk[2].innerHTML);
            var kuantitas = parseInt(idk.find(".kuantitas").text().trim());
            var totalharga = kuantitas * hargasatuan
            
            $.ajax({
                url:"/beli/",
                data:{
                    namabarang: namabarang,
                    hargasatuan: hargasatuan,
                    kuantitas: kuantitas,
                    totalharga: totalharga,
                },
                type: 'POST',
                success: function() {
                    document.getElementById("notifikasi").innerHTML = "Transaksi berhasil" 
                },
                error: function() {
                    document.getElementById("notifikasi").innerHTML = "Transaksi gagal" 
                }
            });
        });
    });

    $(".selected").click(function() {
        $('.tambahkupon').removeAttr('hidden');
    });

    // $(".cari-persen").click(function () {
    //     var select = $(this);
    //     var srh = $(".inputankupon").text();
    //     $.ajax({
    //         url: "/caripersenkupon/",
    //         data: {
    //             srh:srh,
    //         },
    //         type: 'POST',
    //         success: function(result) {
    //             console.log(result.match)
    //         },
    //         error: function (result) {
    //             console.log("gagal ni"); // the message
    //         }
    //     })
    // });
});