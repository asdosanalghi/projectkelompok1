from django.apps import apps
from django.contrib.auth.models import User
from django.test import TestCase, Client, LiveServerTestCase
from .apps import KeranjangConfig
from .models import Keranjang, BarangOrder
from nofaldi_kupon.models import Kupon
from advisTransaksi.models import Transaksi
from wening_barang.models import Barang
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.utils import timezone
import datetime
import time

# Create your tests here.

class UnitTest(TestCase):
    def testApp(self):
        self.assertEqual(KeranjangConfig.name, 'keranjang')
        self.assertEqual(apps.get_app_config('keranjang').name, 'keranjang')

    def test_keranjang_url_exists(self):
        c = Client()
        not_logged_in = self.client.get('/cart/')
        self.assertEqual(not_logged_in['location'],'/')
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()
        c.login(username='test',password='test')
        response = c.get('/cart/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'keranjang.html')

    def test_create_new_keranjang_object(self):
        userx = User.objects.create_user(
            username="aku", email="kamu@kita.com", password="dia")
        cart = Keranjang.objects.create(user=userx)
        barang = Barang.objects.create(
            nama='test', harga='10', kategori='Sneakers', stok='1', deskripsi='Unit testing')
        orderan = BarangOrder.objects.create(
            user=userx, barang=barang, ordered=False)
        cart.isi_keranjang.add(orderan)
        self.assertTrue(isinstance(cart, Keranjang))
        self.assertTrue(isinstance(orderan, BarangOrder))
        self.assertEqual("aku's Shopping Cart", str(cart))
        self.assertEqual("test", str(orderan))
        self.assertEqual('10',orderan.harga())
    
    def test_barang(self):
        barang = Barang.objects.create(
            nama='test', harga='10', kategori='Sneakers', stok='0', deskripsi='Unit testing')
        barang2 = Barang.objects.create(
            nama='test2', harga='10', kategori='Sneakers', stok='2', deskripsi='Unit testing')
        request = self.client.post('/add/',{'namabarang': 'test', 'kuantitas':'2'})
        self.assertEqual(request.status_code, 403)
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()
        c.login(username='test',password='test')
        cart = Keranjang.objects.create(user=user)
        orderan = BarangOrder.objects.create(
            user=user, barang=barang2, ordered=False)
        request2 = c.post('/add/',{'namabarang': 'test2', 'kuantitas':'1'})

    def test_reduce(self):
        barang2 = Barang.objects.create(
            nama='test2', harga='10', kategori='Sneakers', stok='2', deskripsi='Unit testing')
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()
        c.login(username='test',password='test')
        cart = Keranjang.objects.create(user=user)
        orderan = BarangOrder.objects.create(
            user=user, barang=barang2, ordered=False)
        request2 = c.post('/reduce/',{'namabarang': 'test2', 'kuantitas':'1'})
        request3 = c.post('/reduce/',{'namabarang': 'test2', 'kuantitas':'2'})

    def test_remove(self):
        barang2 = Barang.objects.create(
            nama='test2', harga='10', kategori='Sneakers', stok='2', deskripsi='Unit testing')
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()
        c.login(username='test',password='test')
        cart = Keranjang.objects.create(user=user)
        orderan = BarangOrder.objects.create(
            user=user, barang=barang2, ordered=False)
        request3 = c.post('/remove/',{'namabarang': 'test2', 'kuantitas':'2'})

    def test_keranjang(self):
        barang2 = Barang.objects.create(
            nama='test2', harga='10', kategori='Sneakers', stok='2', deskripsi='Unit testing')
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()
        c.login(username='test',password='test')
        new_coupon = Kupon.objects.create(kode="xjahfjh",persen=40,waktu=timezone.now(),minimum=400000)
        cart = Keranjang.objects.create(user=user)
        orderan = BarangOrder.objects.create(
            user=user, barang=barang2, ordered=False)
        req1 = c.post('/cart/',{'srh':40, 'tipecari':'Cari Berdasarkan Persen'})
        self.assertTemplateUsed(req1,'keranjang.html')
        req2 = c.post('/cart/',{'srh':'xjahfjh', 'tipecari':'Cari Berdasarkan Nama'})
        self.assertTemplateUsed(req2,'keranjang.html')
        req3 = c.post('/cart/',{'srh':'xjahfjh', 'selected':'xjahfjh'})
        cart.kupon_used = new_coupon
        req4 = c.post('/cart/',{'srh':'xjahfjh', 'selected':'xjahfjh'})

# class FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         self.browser = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

#     def test_add_new_item_to_cart(self):
#         testbarang = Barang.objects.create(
#             nama="barang1",
#             harga=1000,
#             stok=100,
#             deskripsi="loh",
#         )
#         testbarang.save()

#         user = User.objects.create_user("wening", "wening@wening.com", "123")
#         user.save()

#         # I want to try out this new online shop, let me create an account first
#         self.browser.get('%s%s' % (self.live_server_url, '/login/'))
#         time.sleep(3)
#         nama = self.browser.find_element_by_name('username')
#         nama.send_keys('wening')
#         password = self.browser.find_element_by_name('password')
#         password.send_keys('123')
#         login = self.browser.find_element_by_id('login')
#         login.click()
#         time.sleep(3)

#         # Now that I've got enough money, I want to buy the item I've saved the link before!
#         self.browser.get('%s%s' % (self.live_server_url, '/item/barang1'))
#         time.sleep(3)

#         # I want to add it to my cart
#         add = self.browser.find_element_by_id('cart')
#         add.click()
#         time.sleep(2)
