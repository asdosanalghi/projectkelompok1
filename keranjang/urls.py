from django.urls import path
from . import views


app_name = 'keranjang'

urlpatterns = [
    path('cart/', views.keranjang, name='keranjang'),
    path('add/', views.add_to_keranjang, name='add-to-keranjang'),
    path('reduce/', views.reduce_quantity, name='reduce-quantity'),
    path('remove/', views.remove_from_keranjang,
         name='remove-from-keranjang'),
    # path('usekupon/', views.usekupon, name='use-kupon'),
    # path('carinamakupon/', views.carinamakupon, name='carinamakupon'),
    # path('caripersenkupon/', views.caripersenkupon, name='caripersenkupon'),
    path('beli/', views.beli, name='beli'),
]
