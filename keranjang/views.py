from django.shortcuts import render, get_object_or_404, redirect
from wening_barang.models import Barang
from .models import Keranjang, BarangOrder
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from nofaldi_kupon.models import Kupon
from advisTransaksi.models import Transaksi
import datetime

# Create your views here.


@ csrf_exempt
def keranjang(request):
    if request.method == "POST":
        srch = request.POST.get('srh', None)
        tipe = request.POST.get('tipecari', None)
        kupon_tambah = request.POST.get('selected', None)

        if tipe == "Cari Berdasarkan Persen":
            match = Kupon.objects.filter(persen__contains=srch)
            keranjang = Keranjang.objects.filter(user=request.user)

            map = {
                'keranjang': keranjang,
                'srch': match,
            }

            return render(request, 'keranjang.html', map)
        
        elif tipe == "Cari Berdasarkan Nama":
            match = Kupon.objects.filter(kode__contains=srch)
            keranjang = Keranjang.objects.filter(user=request.user)

            map = {
                'keranjang': keranjang,
                'srch': match,
            }

            return render(request, 'keranjang.html', map)
        
        elif kupon_tambah is not None:
            keranjang = get_object_or_404(Keranjang, user=request.user)
            objkupon = get_object_or_404(Kupon, kode=kupon_tambah)
            if keranjang.kupon_used is None :
                keranjang.kupon_used = objkupon
                keranjang.total_harga = keranjang.total_harga - (keranjang.total_harga*objkupon.persen/100)
                keranjang.save()
                hasil = Keranjang.objects.filter(user=request.user)
                map = {
                    'keranjang': hasil,
                }
                print("berhasil")
                return render(request, 'keranjang.html', map)
            else :
                # keranjang.kupon_used = objkupon
                # keranjang.total_harga = keranjang.total_harga + (keranjang.total_harga*keranjang.kupon_used.persen/100) #dibalikin jadi semula
                keranjang.total_harga = keranjang.total_harga / (1 - (keranjang.kupon_used.persen/100)) #dibalikin semula
                keranjang.kupon_used = objkupon
                keranjang.total_harga = keranjang.total_harga - (keranjang.total_harga*objkupon.persen/100)
                keranjang.save()
                hasil = Keranjang.objects.filter(user=request.user)
                map = {
                    'keranjang': hasil,
                }
                print("berhasil")
                return render(request, 'keranjang.html', map)


    else :
        if request.user.is_authenticated:  
            keranjang = Keranjang.objects.filter(user=request.user)
            # barang = keranjang.objects.all()
            # harga = keranjang.isi_keranjang
            map = {
                'keranjang': keranjang,
            }
            return render(request, 'keranjang.html', map)
        else:
            return redirect('/')

@csrf_exempt
def add_to_keranjang(request):
    # Check if the item exists
    if request.method == "POST":

        nama_barang = request.POST.get('namabarang', None)
        kuantitas = request.POST.get('kuantitas', None)

        barang = get_object_or_404(Barang, nama=nama_barang)

        if barang.stok < 1:
            response = JsonResponse(
                {"error": "Stok barang tidak mencukupi"})
            response.status_code = 403
            return response
        else:
            #   Will create new cart and temporary ordered item
            if int(kuantitas) > barang.stok:
                response = JsonResponse(
                    {"error": "Stok barang tidak mencukupi"})
                response.status_code = 403
                return response
            else:
                order_qs, status = Keranjang.objects.get_or_create(
                    user=request.user)
                barang_order, status_barang = BarangOrder.objects.get_or_create(
                    barang=barang, user=request.user, ordered=False)
                if status_barang == False:
                    if barang_order.kuantitas >= barang.stok:
                        response = JsonResponse(
                            {"error": "Stok barang tidak mencukupi"})
                        response.status_code = 403
                        return response
                    else:
                        barang_order.kuantitas += 1
                        barang_order.save()
                        print(barang_order.harga())
                        if order_qs.kupon_used is None :
                            order_qs.total_harga += barang.harga
                            order_qs.save()
                            print(order_qs.total_harga)
                            print(barang_order.kuantitas*barang.harga)
                            print("kuponnya none")
                            return HttpResponse("Success!")
                        else :
                            order_qs.total_harga = order_qs.total_harga / (1 - (order_qs.kupon_used.persen/100)) #dibalikin semula
                            order_qs.total_harga = order_qs.total_harga + barang.harga
                            order_qs.total_harga = order_qs.total_harga - (order_qs.total_harga*order_qs.kupon_used.persen/100) #apply diskon
                            order_qs.save()
                            return HttpResponse("Success!")
                else:
                    if order_qs.kupon_used is None :
                        order_qs.isi_keranjang.add(barang_order)
                        order_qs.total_harga += barang.harga
                        order_qs.save()
                        print("horray")
                        return HttpResponse("Horray!")
                    else :
                        order_qs.isi_keranjang.add(barang_order)
                        order_qs.total_harga = order_qs.total_harga / (1 - (order_qs.kupon_used.persen/100)) #dibalikin semula
                        order_qs.total_harga = order_qs.total_harga + barang.harga
                        order_qs.total_harga = order_qs.total_harga - (order_qs.total_harga*order_qs.kupon_used.persen/100) #apply diskon
                        order_qs.save()
                        print("horray")
                        return HttpResponse("Horray!")


@ csrf_exempt
def remove_from_keranjang(request):
    # Check if the item exists
    if request.method == "POST":
        nama_barang = request.POST.get('namabarang', None)
        barang = get_object_or_404(Barang, nama=nama_barang)

        order_qs, status = Keranjang.objects.get_or_create(
            user=request.user)
        barang_order, status_barang = BarangOrder.objects.get_or_create(
            barang=barang, user=request.user, ordered=False)

        if status == False and status_barang == False:
            if order_qs.kupon_used is None :
                order_qs.total_harga -= barang.harga*barang_order.kuantitas
                order_qs.save()
                barang_order.delete()
                # Keranjang.isi_keranjang.remove(barang_order)
                return HttpResponse("Success!")
            else :
                order_qs.total_harga = order_qs.total_harga / (1 - (order_qs.kupon_used.persen/100)) #dibalikin semula
                order_qs.total_harga -= barang.harga*barang_order.kuantitas
                order_qs.total_harga = order_qs.total_harga - (order_qs.total_harga*order_qs.kupon_used.persen/100) #apply diskon
                order_qs.save()
                barang_order.delete()
                return HttpResponse("Success!")
        else:
            response = JsonResponse(
                {"error": "Terjadi kesalahan"})
            response.status_code = 403
            return response


@ csrf_exempt
def reduce_quantity(request):
    # Check if the item exists
    if request.method == "POST":
        nama_barang = request.POST.get('namabarang', None)
        kuantitas = request.POST.get('kuantitas', None)

        barang = get_object_or_404(Barang, nama=nama_barang)

        #   Will create new cart and temporary ordered item
        if int(kuantitas) <= 1:
            response = JsonResponse(
                {"error": "Kuantitas tidak bisa nol. Silakan hapus barang dari keranjang"})
            response.status_code = 403
            return response
        else:
            order_qs, status = Keranjang.objects.get_or_create(
                user=request.user)
            barang_order, status_barang = BarangOrder.objects.get_or_create(
                barang=barang, user=request.user, ordered=False)
            if status == False and status_barang == False:
                if order_qs.kupon_used is None :
                    barang_order.kuantitas -= 1
                    order_qs.total_harga -= barang.harga
                    order_qs.save()
                    barang_order.save()
                    return HttpResponse("Success!")
                else :
                    barang_order.kuantitas -= 1
                    order_qs.total_harga = order_qs.total_harga / (1 - (order_qs.kupon_used.persen/100)) #dibalikin semula
                    order_qs.total_harga -= barang.harga
                    order_qs.total_harga = order_qs.total_harga - (order_qs.total_harga*order_qs.kupon_used.persen/100) #apply diskon
                    order_qs.save()
                    barang_order.save()
                    return HttpResponse("Success!")


# def usekupon(request):
#     if request.method == "POST":
#         return


# def carinamakupon(request):
#     if request.method == "POST":
#         srch = request.POST['srh']
#         match = Kupon.objects.filter(kode__contains=srch)

#         return {'srch': match}


# @ csrf_exempt
# def caripersenkupon(request):
#     if request.method == "POST":
#         srch = request.POST.get('srh', None)
#         match = Kupon.objects.filter(persen__contains=srch)
#         keranjang = Keranjang.objects.filter(user=request.user)

#         map = {
#             'keranjang': keranjang,
#             'srch': match,
#         }

#         return render(request, 'keranjang.html', map)

def beli(request):
    if request.method == "POST":
        pembeli = request.user
        namabarang = request.POST.get('namabarang', None)
        hargasatuan = request.POST.get('hargasatuan', None)
        kuantitas = request.POST.get('kuantitas', None)
        totalharga = request.POST.get('totalharga', None)

        transaksi = Transaksi.objects.create(namaPembeli = pembeli,
                                            barangDibeli = namabarang,
                                            totalTransaksi = totalharga,
                                            tanggalBeli = datetime.date(datetime.now()),
                                            kuantitasDibeli = kuantitas)
        transaksi.save()
        return render(request, 'transaksi.html')