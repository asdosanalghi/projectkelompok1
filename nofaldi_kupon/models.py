from django.db import models

# Create your models here.
class Kupon(models.Model):
    kode = models.CharField(max_length=30)
    persen = models.IntegerField(default=0,blank=True,null=True)
    waktu  = models.DateTimeField()
    minimum = models.IntegerField(default=0,blank=True,null=True)

    def __str__(self):
        return self.kode