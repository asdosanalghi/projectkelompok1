from django.test import TestCase, Client
from django.utils import timezone
from .models import Kupon
# Create your tests here.

class UnitTest(TestCase):

    def test_url_exist(self):
        response = self.client.get('/search')
        self.assertEqual(response.status_code, 301)

    def test_page_uses_index_template(self):
        response = Client().get('/')
        response2 = Client().get('/search/')
        self.assertTemplateUsed(response,'navbar.html')
        self.assertTemplateUsed(response2,'kupon.html')

    def test_model_create_new_coupon(self):
        new_coupon = Kupon.objects.create(kode="xjahfjh",persen=40,waktu=timezone.now(),minimum=400000)
        banyak = Kupon.objects.all().count()
        self.assertEqual(banyak,1)
        new_coupon_2 = Kupon.objects.create(kode="ajhfas",persen=20,waktu=timezone.now(),minimum=50000)
        banyak2 = Kupon.objects.all().count()
        self.assertEqual(banyak2,2)

    def test_string_representation(self):
        kupon = Kupon(kode="anjay",persen=40,waktu=timezone.now(),minimum=400000)
        self.assertEqual(str(kupon),kupon.kode)

    def test_page_uses_index_template_after_post(self):
        response = self.client.post('/search/',{'srh':40,})
        self.assertEqual(response.status_code, 200)