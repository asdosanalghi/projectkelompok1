from django.shortcuts import render
from .models import Kupon
from django.contrib import messages
# Create your views here.
def kupon(request):
    if request.method == "POST":
        srch = request.POST['srh']
        match = Kupon.objects.filter(persen__contains = srch)

        return render(request,'kupon.html',{'srch' : match})
    else :
        isi = Kupon.objects.all()
        return render(request,'kupon.html',{'srch':isi})

