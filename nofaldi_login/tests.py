from django.test import TestCase, Client
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class UnitTest(TestCase):

    def test_url(self):
        response1 = self.client.get('/login/')
        self.assertEqual(response1.status_code, 200)
        self.assertTemplateUsed(response1, 'login.html')
        response2 = self.client.get('/login/register/')
        self.assertEqual(response2.status_code, 200)
        self.assertTemplateUsed(response2, 'register.html')
    
    def test_account(self):
        response = self.client.post('/login/register/', {'username':'tested', 'email':'testedacc@email.com', 'password':'tester', 'firstname':'first_test', 'lastname' : 'last_test'})
        self.assertEqual(response['location'], '/login/')
        login = self.client.post('/login/', {'username':'tested', 'password':'tester'})
        self.assertEqual(login['location'], '/')
        self.client.login(username='tested',password='tester')
        logout = self.client.post('/login/logout/')
        self.assertEqual(logout['location'], '/')
        self.client.logout()

class FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_fill_form(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/login/')
        # selenium.find_element_by_id("signup").click()
        # time.sleep(3)
        # selenium.find_element_by_name("username").send_keys("Testing3")
        # selenium.find_element_by_name("firstname").send_keys("Test First Name")
        # selenium.find_element_by_name("lastname").send_keys("Test Last Name")
        # selenium.find_element_by_name("email").send_keys("testing@gmail.com")
        # selenium.find_element_by_name("password").send_keys("Testing")
        # selenium.find_element_by_name("signup").submit()
        time.sleep(3)
        selenium.find_element_by_name("username").send_keys("Testing1")
        selenium.find_element_by_name("password").send_keys("Testing")
        selenium.find_element_by_name("login").submit()
