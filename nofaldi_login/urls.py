from django.urls import path
from . import views

app_name = "login"

urlpatterns = [
    path('', views.loginpage, name = "login-page"),
    path('register/', views.register, name="register"),
    path('logout/', views.logoutpage, name="logout-page")
]