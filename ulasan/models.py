from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Ulasan(models.Model):
    pengulas = models.ForeignKey(User, on_delete=models.CASCADE)
    nama_barang = models.CharField(max_length = 30)
    ulasan = models.TextField()

    def __str__(self):
        return self.pengulas.username