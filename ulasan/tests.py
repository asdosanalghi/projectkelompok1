from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

from .models import Ulasan

import os
import time
import random
import string

# Create your tests here.

class UnitTest(TestCase):
    def test_model(self):
        user = User.objects.create_user(username='testuser', email='testuser@email.com', password='12345')
        ulasan = Ulasan.objects.create(pengulas=user, nama_barang='Tes Barang Baru', ulasan='Tes Barang Baru ini bagus')

        count_ulasan = Ulasan.objects.all().count()
        self.assertEqual(count_ulasan, 1) 

        ulasan.delete()
        user.delete()

        count_ulasan = Ulasan.objects.all().count()
        self.assertEqual(count_ulasan, 0) 

        self.assertEqual(str(ulasan), ulasan.pengulas.username)
        

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        # if 'GITLAB_CI' in os.environ:
        #     self.browser = webdriver.Chrome(
        #         './chromedriver', chrome_options=chrome_options)
        # else:
        #     self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()

    def test_tulis_ulasan(self):
        user = User.objects.create_user(username='testuser', email='testuser@email.com', password='12345')
        self.client = Client()
        self.client.force_login(user)

        self.browser.get(self.live_server_url + '/ulasan/Tes Tulis Barang')
        time.sleep(5) # Menunggu halaman terbuka

        text_area = self.browser.find_element_by_name('ulasan')
        text_area.clear()
        text_area.send_keys("Tes Tulis Barang Berhasil")

        submit_button = self.browser.find_element_by_xpath("//button[@name='submit-btn']")
        self.browser.execute_script("arguments[0].click();", submit_button)
        time.sleep(15)

        assert "Tes Tulis Barang Berhasil"




    

        

    