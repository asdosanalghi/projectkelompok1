# ulasan urls.py
from django.urls import path
from ulasan import views 

app_name = 'ulasan'

urlpatterns = [
    path('<str:nama_barang>/',  views.UlasanListView.as_view(), name='crud_ajax'),
    path('ajax/crud/create/',  views.CreateCrudUser.as_view(), name='crud_ajax_create'),
    path('ajax/crud/delete/',  views.DeleteCrudUser.as_view(), name='crud_ajax_delete'),
]
