from django.views.generic import ListView
from django.shortcuts import render
from .models import Ulasan
from django.views.generic import View
from django.http import JsonResponse

class UlasanListView(View):
    def get(self, request, nama_barang = ''):
        all_ulasan = Ulasan.objects.all()
        context = {'ulasan': all_ulasan, 'user': request.user, 'barang' : nama_barang}
        
        return render(request, "ulasan.html", context)

class CreateCrudUser(View):
    def get(self, request):
        pesan = request.GET.get('ulasan', None)
        nama_barang = request.GET.get('nama_barang')
        user = request.user
        objek = Ulasan.objects.create(pengulas = user, nama_barang = nama_barang, ulasan = pesan)
        
        ulasan = {'id': objek.id, 'pengulas': objek.pengulas.username, 'ulasan': pesan, 'userLoggedIn': user.username, 'nama_barang': nama_barang}
        data = {'ulasan': ulasan}
        return JsonResponse(data)

class DeleteCrudUser(View):
    def get(self,request):
        id = request.GET.get('id', None)
        Ulasan.objects.get(id = id).delete()
        data = {'deleted': True}
        return JsonResponse(data)