from django.shortcuts import render
from wening_barang.models import Barang
# Create your views here.


def homepage(request):
    barang = Barang.objects.all()
    map = {
        'barang' : barang,
    }
    return render(request, 'homepage.html',map)
