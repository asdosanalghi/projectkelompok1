from django.contrib import admin
from .models import Barang

# Register your models here.


class BarangAdmin(admin.ModelAdmin):
    list_display = ('nama', 'kategori', 'harga', 'stok', 'deskripsi')
    ordering = ['kategori', 'nama']


admin.site.register(Barang, BarangAdmin)
