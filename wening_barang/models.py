from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.


class Barang(models.Model):
    CATEGORY_CHOICES = (
        ('Sneakers', 'Sneakers'),
        ('Boots', 'Boots'),
        ('Sandal', 'Sandal'),
        ('Slip On', 'Slip On'),
        ('Wedges', 'Wedges'),
        ('Flats', 'Flats'),
        ('Heels', 'Heels')
    )

    nama = models.CharField('Nama Barang', max_length=120, unique=True, null=False)
    harga = models.PositiveIntegerField(
        'Harga', validators=[MinValueValidator(1)], default=0)
    kategori = models.CharField(
        choices=CATEGORY_CHOICES, default='Sneakers', max_length=120)
    stok = models.PositiveIntegerField(
        'Stok', validators=[MinValueValidator(1)], default=0)
    deskripsi = models.TextField('Deskripsi Barang')

    def __str__(self):
        return self.nama
