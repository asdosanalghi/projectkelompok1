from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import WeningBarangConfig
from .models import Barang
from .views import barang_detail

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(WeningBarangConfig.name, 'wening_barang')
        self.assertEqual(apps.get_app_config(
            'wening_barang').name, 'wening_barang')


class BarangTest(TestCase):
    def test_barang_create_new_item(self):
        item_new = Barang.objects.create(
            nama='test', harga='10', kategori='Sneakers', stok='1', deskripsi='Unit testing')
        self.assertTrue(isinstance(item_new, Barang))
        self.assertEqual('test', str(item_new))
        jumlah = Barang.objects.all().count()
        self.assertEqual(jumlah, 1)
        respone = Client().get('/item/' + item_new.nama + '/')
        self.assertEqual(respone.status_code, 200)
