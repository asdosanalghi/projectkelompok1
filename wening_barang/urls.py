from django.urls import path
from . import views

app_name = 'wening_barang'

urlpatterns = [
    path('<str:nama_barang>/', views.barang_detail, name='barang_detail')
]
