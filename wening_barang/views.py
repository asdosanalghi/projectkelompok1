from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Barang

# Create your views here.

def barang_detail(request, nama_barang=''):
    barang = Barang.objects.get(nama=nama_barang)
    return render(request, 'barang_detail.html', {'barang': barang})
